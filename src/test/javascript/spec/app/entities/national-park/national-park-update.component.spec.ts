/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { JhipsterTestModule } from '../../../test.module';
import { NationalParkUpdateComponent } from 'app/entities/national-park/national-park-update.component';
import { NationalParkService } from 'app/entities/national-park/national-park.service';
import { NationalPark } from 'app/shared/model/national-park.model';

describe('Component Tests', () => {
    describe('NationalPark Management Update Component', () => {
        let comp: NationalParkUpdateComponent;
        let fixture: ComponentFixture<NationalParkUpdateComponent>;
        let service: NationalParkService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [JhipsterTestModule],
                declarations: [NationalParkUpdateComponent]
            })
                .overrideTemplate(NationalParkUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(NationalParkUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(NationalParkService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new NationalPark(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.nationalPark = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new NationalPark();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.nationalPark = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
