/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { JhipsterTestModule } from '../../../test.module';
import { NationalParkDetailComponent } from 'app/entities/national-park/national-park-detail.component';
import { NationalPark } from 'app/shared/model/national-park.model';

describe('Component Tests', () => {
    describe('NationalPark Management Detail Component', () => {
        let comp: NationalParkDetailComponent;
        let fixture: ComponentFixture<NationalParkDetailComponent>;
        const route = ({ data: of({ nationalPark: new NationalPark(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [JhipsterTestModule],
                declarations: [NationalParkDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(NationalParkDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(NationalParkDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.nationalPark).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
