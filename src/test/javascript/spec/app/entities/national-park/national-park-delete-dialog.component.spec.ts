/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { JhipsterTestModule } from '../../../test.module';
import { NationalParkDeleteDialogComponent } from 'app/entities/national-park/national-park-delete-dialog.component';
import { NationalParkService } from 'app/entities/national-park/national-park.service';

describe('Component Tests', () => {
    describe('NationalPark Management Delete Component', () => {
        let comp: NationalParkDeleteDialogComponent;
        let fixture: ComponentFixture<NationalParkDeleteDialogComponent>;
        let service: NationalParkService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [JhipsterTestModule],
                declarations: [NationalParkDeleteDialogComponent]
            })
                .overrideTemplate(NationalParkDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(NationalParkDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(NationalParkService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
