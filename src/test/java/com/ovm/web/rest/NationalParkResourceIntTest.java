package com.ovm.web.rest;

import com.ovm.JhipsterApp;

import com.ovm.domain.NationalPark;
import com.ovm.repository.NationalParkRepository;
import com.ovm.service.NationalParkService;
import com.ovm.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.persistence.EntityManager;
import java.util.List;


import static com.ovm.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the NationalParkResource REST controller.
 *
 * @see NationalParkResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = JhipsterApp.class)
public class NationalParkResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LOCATION = "AAAAAAAAAA";
    private static final String UPDATED_LOCATION = "BBBBBBBBBB";

    private static final String DEFAULT_CAPACITY = "AAAAAAAAAA";
    private static final String UPDATED_CAPACITY = "BBBBBBBBBB";

    private static final byte[] DEFAULT_PICTURE = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_PICTURE = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_PICTURE_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_PICTURE_CONTENT_TYPE = "image/png";

    @Autowired
    private NationalParkRepository nationalParkRepository;
    
    @Autowired
    private NationalParkService nationalParkService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restNationalParkMockMvc;

    private NationalPark nationalPark;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final NationalParkResource nationalParkResource = new NationalParkResource(nationalParkService);
        this.restNationalParkMockMvc = MockMvcBuilders.standaloneSetup(nationalParkResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NationalPark createEntity(EntityManager em) {
        NationalPark nationalPark = new NationalPark()
            .name(DEFAULT_NAME)
            .location(DEFAULT_LOCATION)
            .capacity(DEFAULT_CAPACITY)
            .picture(DEFAULT_PICTURE)
            .pictureContentType(DEFAULT_PICTURE_CONTENT_TYPE);
        return nationalPark;
    }

    @Before
    public void initTest() {
        nationalPark = createEntity(em);
    }

    @Test
    @Transactional
    public void createNationalPark() throws Exception {
        int databaseSizeBeforeCreate = nationalParkRepository.findAll().size();

        // Create the NationalPark
        restNationalParkMockMvc.perform(post("/api/national-parks")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(nationalPark)))
            .andExpect(status().isCreated());

        // Validate the NationalPark in the database
        List<NationalPark> nationalParkList = nationalParkRepository.findAll();
        assertThat(nationalParkList).hasSize(databaseSizeBeforeCreate + 1);
        NationalPark testNationalPark = nationalParkList.get(nationalParkList.size() - 1);
        assertThat(testNationalPark.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testNationalPark.getLocation()).isEqualTo(DEFAULT_LOCATION);
        assertThat(testNationalPark.getCapacity()).isEqualTo(DEFAULT_CAPACITY);
        assertThat(testNationalPark.getPicture()).isEqualTo(DEFAULT_PICTURE);
        assertThat(testNationalPark.getPictureContentType()).isEqualTo(DEFAULT_PICTURE_CONTENT_TYPE);
    }

    @Test
    @Transactional
    public void createNationalParkWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = nationalParkRepository.findAll().size();

        // Create the NationalPark with an existing ID
        nationalPark.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restNationalParkMockMvc.perform(post("/api/national-parks")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(nationalPark)))
            .andExpect(status().isBadRequest());

        // Validate the NationalPark in the database
        List<NationalPark> nationalParkList = nationalParkRepository.findAll();
        assertThat(nationalParkList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = nationalParkRepository.findAll().size();
        // set the field null
        nationalPark.setName(null);

        // Create the NationalPark, which fails.

        restNationalParkMockMvc.perform(post("/api/national-parks")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(nationalPark)))
            .andExpect(status().isBadRequest());

        List<NationalPark> nationalParkList = nationalParkRepository.findAll();
        assertThat(nationalParkList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLocationIsRequired() throws Exception {
        int databaseSizeBeforeTest = nationalParkRepository.findAll().size();
        // set the field null
        nationalPark.setLocation(null);

        // Create the NationalPark, which fails.

        restNationalParkMockMvc.perform(post("/api/national-parks")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(nationalPark)))
            .andExpect(status().isBadRequest());

        List<NationalPark> nationalParkList = nationalParkRepository.findAll();
        assertThat(nationalParkList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCapacityIsRequired() throws Exception {
        int databaseSizeBeforeTest = nationalParkRepository.findAll().size();
        // set the field null
        nationalPark.setCapacity(null);

        // Create the NationalPark, which fails.

        restNationalParkMockMvc.perform(post("/api/national-parks")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(nationalPark)))
            .andExpect(status().isBadRequest());

        List<NationalPark> nationalParkList = nationalParkRepository.findAll();
        assertThat(nationalParkList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllNationalParks() throws Exception {
        // Initialize the database
        nationalParkRepository.saveAndFlush(nationalPark);

        // Get all the nationalParkList
        restNationalParkMockMvc.perform(get("/api/national-parks?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(nationalPark.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].location").value(hasItem(DEFAULT_LOCATION.toString())))
            .andExpect(jsonPath("$.[*].capacity").value(hasItem(DEFAULT_CAPACITY.toString())))
            .andExpect(jsonPath("$.[*].pictureContentType").value(hasItem(DEFAULT_PICTURE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].picture").value(hasItem(Base64Utils.encodeToString(DEFAULT_PICTURE))));
    }
    
    @Test
    @Transactional
    public void getNationalPark() throws Exception {
        // Initialize the database
        nationalParkRepository.saveAndFlush(nationalPark);

        // Get the nationalPark
        restNationalParkMockMvc.perform(get("/api/national-parks/{id}", nationalPark.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(nationalPark.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.location").value(DEFAULT_LOCATION.toString()))
            .andExpect(jsonPath("$.capacity").value(DEFAULT_CAPACITY.toString()))
            .andExpect(jsonPath("$.pictureContentType").value(DEFAULT_PICTURE_CONTENT_TYPE))
            .andExpect(jsonPath("$.picture").value(Base64Utils.encodeToString(DEFAULT_PICTURE)));
    }

    @Test
    @Transactional
    public void getNonExistingNationalPark() throws Exception {
        // Get the nationalPark
        restNationalParkMockMvc.perform(get("/api/national-parks/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNationalPark() throws Exception {
        // Initialize the database
        nationalParkService.save(nationalPark);

        int databaseSizeBeforeUpdate = nationalParkRepository.findAll().size();

        // Update the nationalPark
        NationalPark updatedNationalPark = nationalParkRepository.findById(nationalPark.getId()).get();
        // Disconnect from session so that the updates on updatedNationalPark are not directly saved in db
        em.detach(updatedNationalPark);
        updatedNationalPark
            .name(UPDATED_NAME)
            .location(UPDATED_LOCATION)
            .capacity(UPDATED_CAPACITY)
            .picture(UPDATED_PICTURE)
            .pictureContentType(UPDATED_PICTURE_CONTENT_TYPE);

        restNationalParkMockMvc.perform(put("/api/national-parks")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedNationalPark)))
            .andExpect(status().isOk());

        // Validate the NationalPark in the database
        List<NationalPark> nationalParkList = nationalParkRepository.findAll();
        assertThat(nationalParkList).hasSize(databaseSizeBeforeUpdate);
        NationalPark testNationalPark = nationalParkList.get(nationalParkList.size() - 1);
        assertThat(testNationalPark.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testNationalPark.getLocation()).isEqualTo(UPDATED_LOCATION);
        assertThat(testNationalPark.getCapacity()).isEqualTo(UPDATED_CAPACITY);
        assertThat(testNationalPark.getPicture()).isEqualTo(UPDATED_PICTURE);
        assertThat(testNationalPark.getPictureContentType()).isEqualTo(UPDATED_PICTURE_CONTENT_TYPE);
    }

    @Test
    @Transactional
    public void updateNonExistingNationalPark() throws Exception {
        int databaseSizeBeforeUpdate = nationalParkRepository.findAll().size();

        // Create the NationalPark

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restNationalParkMockMvc.perform(put("/api/national-parks")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(nationalPark)))
            .andExpect(status().isBadRequest());

        // Validate the NationalPark in the database
        List<NationalPark> nationalParkList = nationalParkRepository.findAll();
        assertThat(nationalParkList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteNationalPark() throws Exception {
        // Initialize the database
        nationalParkService.save(nationalPark);

        int databaseSizeBeforeDelete = nationalParkRepository.findAll().size();

        // Get the nationalPark
        restNationalParkMockMvc.perform(delete("/api/national-parks/{id}", nationalPark.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<NationalPark> nationalParkList = nationalParkRepository.findAll();
        assertThat(nationalParkList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(NationalPark.class);
        NationalPark nationalPark1 = new NationalPark();
        nationalPark1.setId(1L);
        NationalPark nationalPark2 = new NationalPark();
        nationalPark2.setId(nationalPark1.getId());
        assertThat(nationalPark1).isEqualTo(nationalPark2);
        nationalPark2.setId(2L);
        assertThat(nationalPark1).isNotEqualTo(nationalPark2);
        nationalPark1.setId(null);
        assertThat(nationalPark1).isNotEqualTo(nationalPark2);
    }
}
