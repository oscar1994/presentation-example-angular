export interface INationalPark {
    id?: number;
    name?: string;
    location?: string;
    capacity?: string;
    pictureContentType?: string;
    picture?: any;
}

export class NationalPark implements INationalPark {
    constructor(
        public id?: number,
        public name?: string,
        public location?: string,
        public capacity?: string,
        public pictureContentType?: string,
        public picture?: any
    ) {}
}
