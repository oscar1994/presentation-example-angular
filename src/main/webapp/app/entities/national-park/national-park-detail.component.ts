import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiDataUtils } from 'ng-jhipster';

import { INationalPark } from 'app/shared/model/national-park.model';

@Component({
    selector: 'jhi-national-park-detail',
    templateUrl: './national-park-detail.component.html'
})
export class NationalParkDetailComponent implements OnInit {
    nationalPark: INationalPark;

    constructor(private dataUtils: JhiDataUtils, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ nationalPark }) => {
            this.nationalPark = nationalPark;
        });
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        window.history.back();
    }
}
