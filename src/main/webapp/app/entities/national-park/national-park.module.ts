import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JhipsterSharedModule } from 'app/shared';
import {
    NationalParkComponent,
    NationalParkDetailComponent,
    NationalParkUpdateComponent,
    NationalParkDeletePopupComponent,
    NationalParkDeleteDialogComponent,
    nationalParkRoute,
    nationalParkPopupRoute
} from './';

const ENTITY_STATES = [...nationalParkRoute, ...nationalParkPopupRoute];

@NgModule({
    imports: [JhipsterSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        NationalParkComponent,
        NationalParkDetailComponent,
        NationalParkUpdateComponent,
        NationalParkDeleteDialogComponent,
        NationalParkDeletePopupComponent
    ],
    entryComponents: [
        NationalParkComponent,
        NationalParkUpdateComponent,
        NationalParkDeleteDialogComponent,
        NationalParkDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JhipsterNationalParkModule {}
