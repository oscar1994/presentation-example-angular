import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { NationalPark } from 'app/shared/model/national-park.model';
import { NationalParkService } from './national-park.service';
import { NationalParkComponent } from './national-park.component';
import { NationalParkDetailComponent } from './national-park-detail.component';
import { NationalParkUpdateComponent } from './national-park-update.component';
import { NationalParkDeletePopupComponent } from './national-park-delete-dialog.component';
import { INationalPark } from 'app/shared/model/national-park.model';

@Injectable({ providedIn: 'root' })
export class NationalParkResolve implements Resolve<INationalPark> {
    constructor(private service: NationalParkService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((nationalPark: HttpResponse<NationalPark>) => nationalPark.body));
        }
        return of(new NationalPark());
    }
}

export const nationalParkRoute: Routes = [
    {
        path: 'national-park',
        component: NationalParkComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'jhipsterApp.nationalPark.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'national-park/:id/view',
        component: NationalParkDetailComponent,
        resolve: {
            nationalPark: NationalParkResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jhipsterApp.nationalPark.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'national-park/new',
        component: NationalParkUpdateComponent,
        resolve: {
            nationalPark: NationalParkResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jhipsterApp.nationalPark.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'national-park/:id/edit',
        component: NationalParkUpdateComponent,
        resolve: {
            nationalPark: NationalParkResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jhipsterApp.nationalPark.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const nationalParkPopupRoute: Routes = [
    {
        path: 'national-park/:id/delete',
        component: NationalParkDeletePopupComponent,
        resolve: {
            nationalPark: NationalParkResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jhipsterApp.nationalPark.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
