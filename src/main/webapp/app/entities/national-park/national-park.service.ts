import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { INationalPark } from 'app/shared/model/national-park.model';

type EntityResponseType = HttpResponse<INationalPark>;
type EntityArrayResponseType = HttpResponse<INationalPark[]>;

@Injectable({ providedIn: 'root' })
export class NationalParkService {
    public resourceUrl = SERVER_API_URL + 'api/national-parks';

    constructor(private http: HttpClient) {}

    create(nationalPark: INationalPark): Observable<EntityResponseType> {
        return this.http.post<INationalPark>(this.resourceUrl, nationalPark, { observe: 'response' });
    }

    update(nationalPark: INationalPark): Observable<EntityResponseType> {
        return this.http.put<INationalPark>(this.resourceUrl, nationalPark, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<INationalPark>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<INationalPark[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
