import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { INationalPark } from 'app/shared/model/national-park.model';
import { NationalParkService } from './national-park.service';

@Component({
    selector: 'jhi-national-park-delete-dialog',
    templateUrl: './national-park-delete-dialog.component.html'
})
export class NationalParkDeleteDialogComponent {
    nationalPark: INationalPark;

    constructor(
        private nationalParkService: NationalParkService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.nationalParkService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'nationalParkListModification',
                content: 'Deleted an nationalPark'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-national-park-delete-popup',
    template: ''
})
export class NationalParkDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ nationalPark }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(NationalParkDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.nationalPark = nationalPark;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
