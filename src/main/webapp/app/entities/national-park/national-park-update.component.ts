import { Component, OnInit, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiDataUtils } from 'ng-jhipster';

import { INationalPark } from 'app/shared/model/national-park.model';
import { NationalParkService } from './national-park.service';

@Component({
    selector: 'jhi-national-park-update',
    templateUrl: './national-park-update.component.html'
})
export class NationalParkUpdateComponent implements OnInit {
    nationalPark: INationalPark;
    isSaving: boolean;

    constructor(
        private dataUtils: JhiDataUtils,
        private nationalParkService: NationalParkService,
        private elementRef: ElementRef,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ nationalPark }) => {
            this.nationalPark = nationalPark;
        });
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    clearInputImage(field: string, fieldContentType: string, idInput: string) {
        this.dataUtils.clearInputImage(this.nationalPark, this.elementRef, field, fieldContentType, idInput);
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.nationalPark.id !== undefined) {
            this.subscribeToSaveResponse(this.nationalParkService.update(this.nationalPark));
        } else {
            this.subscribeToSaveResponse(this.nationalParkService.create(this.nationalPark));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<INationalPark>>) {
        result.subscribe((res: HttpResponse<INationalPark>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
}
