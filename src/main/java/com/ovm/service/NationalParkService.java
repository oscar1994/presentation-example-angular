package com.ovm.service;

import com.ovm.domain.NationalPark;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing NationalPark.
 */
public interface NationalParkService {

    /**
     * Save a nationalPark.
     *
     * @param nationalPark the entity to save
     * @return the persisted entity
     */
    NationalPark save(NationalPark nationalPark);

    /**
     * Get all the nationalParks.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<NationalPark> findAll(Pageable pageable);


    /**
     * Get the "id" nationalPark.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<NationalPark> findOne(Long id);

    /**
     * Delete the "id" nationalPark.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
