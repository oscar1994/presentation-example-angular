package com.ovm.service.impl;

import com.ovm.service.NationalParkService;
import com.ovm.domain.NationalPark;
import com.ovm.repository.NationalParkRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing NationalPark.
 */
@Service
@Transactional
public class NationalParkServiceImpl implements NationalParkService {

    private final Logger log = LoggerFactory.getLogger(NationalParkServiceImpl.class);

    private NationalParkRepository nationalParkRepository;

    public NationalParkServiceImpl(NationalParkRepository nationalParkRepository) {
        this.nationalParkRepository = nationalParkRepository;
    }

    /**
     * Save a nationalPark.
     *
     * @param nationalPark the entity to save
     * @return the persisted entity
     */
    @Override
    public NationalPark save(NationalPark nationalPark) {
        log.debug("Request to save NationalPark : {}", nationalPark);
        return nationalParkRepository.save(nationalPark);
    }

    /**
     * Get all the nationalParks.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<NationalPark> findAll(Pageable pageable) {
        log.debug("Request to get all NationalParks");
        return nationalParkRepository.findAll(pageable);
    }


    /**
     * Get one nationalPark by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<NationalPark> findOne(Long id) {
        log.debug("Request to get NationalPark : {}", id);
        return nationalParkRepository.findById(id);
    }

    /**
     * Delete the nationalPark by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete NationalPark : {}", id);
        nationalParkRepository.deleteById(id);
    }
}
