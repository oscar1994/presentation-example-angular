package com.ovm.repository;

import com.ovm.domain.NationalPark;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the NationalPark entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NationalParkRepository extends JpaRepository<NationalPark, Long> {

}
