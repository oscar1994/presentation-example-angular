/**
 * View Models used by Spring MVC REST controllers.
 */
package com.ovm.web.rest.vm;
