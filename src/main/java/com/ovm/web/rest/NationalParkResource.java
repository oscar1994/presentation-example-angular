package com.ovm.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.ovm.domain.NationalPark;
import com.ovm.service.NationalParkService;
import com.ovm.web.rest.errors.BadRequestAlertException;
import com.ovm.web.rest.util.HeaderUtil;
import com.ovm.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing NationalPark.
 */
@RestController
@RequestMapping("/api")
public class NationalParkResource {

    private final Logger log = LoggerFactory.getLogger(NationalParkResource.class);

    private static final String ENTITY_NAME = "nationalPark";

    private NationalParkService nationalParkService;

    public NationalParkResource(NationalParkService nationalParkService) {
        this.nationalParkService = nationalParkService;
    }

    /**
     * POST  /national-parks : Create a new nationalPark.
     *
     * @param nationalPark the nationalPark to create
     * @return the ResponseEntity with status 201 (Created) and with body the new nationalPark, or with status 400 (Bad Request) if the nationalPark has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/national-parks")
    @Timed
    public ResponseEntity<NationalPark> createNationalPark(@Valid @RequestBody NationalPark nationalPark) throws URISyntaxException {
        log.debug("REST request to save NationalPark : {}", nationalPark);
        if (nationalPark.getId() != null) {
            throw new BadRequestAlertException("A new nationalPark cannot already have an ID", ENTITY_NAME, "idexists");
        }
        NationalPark result = nationalParkService.save(nationalPark);
        return ResponseEntity.created(new URI("/api/national-parks/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /national-parks : Updates an existing nationalPark.
     *
     * @param nationalPark the nationalPark to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated nationalPark,
     * or with status 400 (Bad Request) if the nationalPark is not valid,
     * or with status 500 (Internal Server Error) if the nationalPark couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/national-parks")
    @Timed
    public ResponseEntity<NationalPark> updateNationalPark(@Valid @RequestBody NationalPark nationalPark) throws URISyntaxException {
        log.debug("REST request to update NationalPark : {}", nationalPark);
        if (nationalPark.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        NationalPark result = nationalParkService.save(nationalPark);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, nationalPark.getId().toString()))
            .body(result);
    }

    /**
     * GET  /national-parks : get all the nationalParks.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of nationalParks in body
     */
    @GetMapping("/national-parks")
    @Timed
    public ResponseEntity<List<NationalPark>> getAllNationalParks(Pageable pageable) {
        log.debug("REST request to get a page of NationalParks");
        Page<NationalPark> page = nationalParkService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/national-parks");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /national-parks/:id : get the "id" nationalPark.
     *
     * @param id the id of the nationalPark to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the nationalPark, or with status 404 (Not Found)
     */
    @GetMapping("/national-parks/{id}")
    @Timed
    public ResponseEntity<NationalPark> getNationalPark(@PathVariable Long id) {
        log.debug("REST request to get NationalPark : {}", id);
        Optional<NationalPark> nationalPark = nationalParkService.findOne(id);
        return ResponseUtil.wrapOrNotFound(nationalPark);
    }

    /**
     * DELETE  /national-parks/:id : delete the "id" nationalPark.
     *
     * @param id the id of the nationalPark to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/national-parks/{id}")
    @Timed
    public ResponseEntity<Void> deleteNationalPark(@PathVariable Long id) {
        log.debug("REST request to delete NationalPark : {}", id);
        nationalParkService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
